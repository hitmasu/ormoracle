﻿using OracleTest.Model;
using ORMOracle;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ORMOracle.Components;
using Oracle.ManagedDataAccess.Client;

namespace OracleTest
{
    [Table("Postagens")]
    public class Postagem
    {
        [Column(OracleDbType.Int32, nameof(Id), IsPrimaryKey = true)]
        public int Id { get; set; }

        [Column(OracleDbType.NVarchar2, nameof(Descricao), Size = 200)]
        public string Descricao { get; set; }

        [Column(OracleDbType.Blob, nameof(Image))]
        public byte[] Image { get; set; }

        [Column(OracleDbType.NVarchar2, nameof(Local), Size = 100)]
        public string Local { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {

            Task.Run(async () =>
            {
                //Acesso ao banco de dados
                DataAccess access = new DataAccess("localhost", 1521, "xe", "flavio", "123456");

                //Lista de tabelas que vão ser criadas no Banco (Tem que ser em ordem de dependencia - De cima para baixo)
                IList<object> tables = new List<object>
                {
                    new Postagem()
                };

                //Cria o banco de dados
                await access.CreateDatabaseAsync(tables);

                //Valores simples para teste
                Postagem postagemNumeroUm = new Postagem
                {
                    Descricao = "Olá 1"
                };

                //Valores simples para teste
                Postagem postagemNumeroDois = new Postagem
                {
                    Descricao = "Olá 2"
                };

                //Faz a inserção dos valores no banco
                await access.InsertAsync(postagemNumeroUm);
                await access.InsertAsync(postagemNumeroDois);

                //Obtém os valores do banco
                IList<Postagem> postagens = await access.GetAsync<Postagem>();

                //Faz um Update no banco
                postagemNumeroUm = postagens.First();
                postagemNumeroUm.Descricao = "Olá Alterado";
                await access.UpdateAsync(postagemNumeroUm);

                //Faz um delete no banco do objeto.
                await access.DeleteAsync(postagens[1]);
            }).Wait();
        }
    }
}
