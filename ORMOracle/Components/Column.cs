﻿using Oracle.ManagedDataAccess.Client;
using System;

namespace ORMOracle.Components
{
    public class Column : Attribute
    {
        public OracleDbType Type { get; set; }
        public string ParameterName { get; set; }
        public object Value { get; set; }
        public int Size { get; set; }
        public bool IsPrimaryKey { get; set; }

        public Column(OracleDbType type, string parameterName, int size = 0)
        {
            Type = type;
            ParameterName = parameterName;
            Size = size;
        }

        public override string ToString()
        {
            string type = string.Empty;
           

            switch (Type)
            {
                case OracleDbType.Int16: case OracleDbType.Int32:
                    type = "Number";
                    break;

                default:
                    type = Type.ToString();
                    break;
            }

            string info = $"{ParameterName} {type}";

            if (Size > 0)
            {
                info += $"({Size})";
            }

            if (IsPrimaryKey)
            {
                info += " GENERATED ALWAYS AS IDENTITY START WITH 1 INCREMENT BY 1";
            }

            return info;
        }
    }
}
