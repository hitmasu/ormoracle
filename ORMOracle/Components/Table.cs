﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORMOracle.Components
{
    public class Table : Attribute
    {
        public Table(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}
