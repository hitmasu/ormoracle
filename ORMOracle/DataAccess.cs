﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using ORMOracle.Components;

namespace ORMOracle
{
    public class DataAccess
    {
        private static OracleConnection _connection;

        public DataAccess(string host, int port, string sid, string userId, string password)
        {
            string source = $"{host}:{port}/{sid}";

            _connection = new OracleConnection
            {
                ConnectionString = new OracleConnectionStringBuilder
                {
                    UserID = userId,
                    Password = password,
                    DataSource = source
                }.ConnectionString
            };
            _connection.Open();
        }

        public async Task CreateDatabaseAsync(IList<object> tables)
        {
            foreach (object table in tables)
            {
                Table tableAttribute = table.GetType().GetCustomAttribute<Table>(false);

                if (tableAttribute == null)
                {
                    throw new Exception($"O tipo {table.GetType()} não possui o atributo {typeof(Table)}!");
                }

                StringBuilder sb = new StringBuilder($"CREATE TABLE {tableAttribute.Name}(");

                IList<PropertyAttribute<Column>> properties = GetPropertiesOfType<Column>(table).ToList();

                for (var i = 0; i < properties.Count; i++)
                {
                    PropertyAttribute<Column> property = properties[i];
                    Column column = property.Attribute;
                    sb.Append($"{column}");

                    if (i < properties.Count - 1)
                    {
                        sb.Append(",");
                    }
                }

                sb.Append(")");

                string sql = sb.ToString();

                //Hack to fix duplicate table
                await ExecuteNonQueryAsync($"DROP TABLE {tableAttribute.Name}");

                await ExecuteNonQueryAsync(sql);
            }
        }

        public async Task InsertAsync<T>(T objToInsert)
        {
            Table tableAttribute = objToInsert.GetType().GetCustomAttribute<Table>(false);
            IList<Column> columns = GetPropertyValues(objToInsert).ToList();
            IList<Column> columnsInQuery = new List<Column>();

            string sql = $"INSERT INTO {tableAttribute.Name}";

            string columnsQuery = "(";
            string columnsValue = " VALUES (";

            foreach (Column column in columns)
            {
                if (column.Value == null || column.IsPrimaryKey)
                {
                    continue;
                }
                columnsInQuery.Add(column);

                columnsQuery += $"{column.ParameterName},";
                columnsValue += $":{column.ParameterName},";
            }

            columnsQuery = columnsQuery.Remove(columnsQuery.Length - 1);
            columnsValue = columnsValue.Remove(columnsValue.Length - 1);

            columnsQuery += ")";
            columnsValue += ")";

            sql += $"{columnsQuery} {columnsValue}";
            await ExecuteNonQueryAsync(sql, columnsInQuery);
        }

        public async Task DeleteAsync<T>(T objToDelete)
        {
            Table tableAttribute = objToDelete.GetType().GetCustomAttribute<Table>(false);
            IList<Column> columns = GetPropertyValues(objToDelete).ToList();
            Column primaryKey = columns.First(x => x.IsPrimaryKey);
            IList<Column> columnsInQuery = new List<Column>
            {
                primaryKey
            };

            string sql = $"DELETE FROM {tableAttribute.Name} WHERE {primaryKey.ParameterName} = :{primaryKey.ParameterName}";
            await ExecuteNonQueryAsync(sql,columnsInQuery);
        }

        public async Task UpdateAsync<T>(T objToUpdate)
        {
            Table tableAttribute = objToUpdate.GetType().GetCustomAttribute<Table>(false);
            IList<Column> columns = GetPropertyValues(objToUpdate).ToList();
            Column primaryKey = columns.First(x => x.IsPrimaryKey);
            IList<Column> columnsInQuery = new List<Column>();

            string sql = $"UPDATE {tableAttribute.Name} SET ";
            string values = string.Empty;

            foreach (Column column in columns)
            {
                if (column.Value == null || column.IsPrimaryKey)
                {
                    continue;
                }

                columnsInQuery.Add(column);

                if (!string.IsNullOrEmpty(values))
                {
                    values += ", AND ";
                }

                values += $"{column.ParameterName} = :{column.ParameterName}";
            }

            values = values.Replace($", AND {Environment.NewLine}",string.Empty);
            sql += $"{values} WHERE {primaryKey.ParameterName} = {primaryKey.Value}";
            await ExecuteNonQueryAsync(sql,columnsInQuery);
        }

        public async Task<IList<T>> GetAsync<T>()
        {
            T instance = Activator.CreateInstance<T>();
            Table tableAttribute = instance.GetType().GetCustomAttribute<Table>(false);
            string sql = $"SELECT * FROM {tableAttribute.Name}";
            DbDataReader reader = await ExecuteQueryAsync(sql);

            IList<PropertyAttribute<Column>> properties = GetPropertiesOfType<Column>(instance).ToList();
            IList<T> rows = new List<T>();

            while (await reader.ReadAsync())
            {
                T rowInstance = Activator.CreateInstance<T>();
                foreach (PropertyAttribute<Column> property in properties)
                {
                    Column column = property.Attribute;
                    int ordinal = reader.GetOrdinal(column.ParameterName);
                    object value = null;

                    if (!await reader.IsDBNullAsync(ordinal))
                    {
                        switch (column.Type)
                        {
                            case OracleDbType.NVarchar2:
                                value = await reader.GetFieldValueAsync<string>(ordinal);
                                break;

                            case OracleDbType.Int32:
                                decimal temporaryDecimal = await reader.GetFieldValueAsync<decimal>(ordinal);
                                value = Convert.ToInt32(temporaryDecimal);
                                break;

                            case OracleDbType.TimeStamp:
                                value = await reader.GetFieldValueAsync<DateTime>(ordinal);
                                break;
                        }
                    }

                    property.Property.SetValue(rowInstance, value);
                }
                rows.Add(rowInstance);
            }

            return rows;
        }


        public async Task ExecuteNonQueryAsync(string sql, IList<Column> columns)
        {
            using (OracleCommand command = _connection.CreateCommand())
            {
                command.CommandText = sql;

                foreach (Column column in columns)
                {
                    command.Parameters.Add(new OracleParameter
                    {
                        ParameterName = column.ParameterName,
                        Value = column.Value,
                        OracleDbType = column.Type
                    });
                }

                await command.ExecuteNonQueryAsync();
            }
        }

        public async Task ExecuteNonQueryAsync(string sql)
        {
            using (OracleCommand command = _connection.CreateCommand())
            {
                command.CommandText = sql;
                await command.ExecuteNonQueryAsync();
            }
        }

        public async Task<DbDataReader> ExecuteQueryAsync(string sql)
        {
            using (OracleCommand command = _connection.CreateCommand())
            {
                command.CommandText = sql;
                DbDataReader reader = await command.ExecuteReaderAsync(CommandBehavior.CloseConnection);
                return reader;
            }
        }

        private static IEnumerable<Column> GetPropertyValues(object obj)
        {
            IEnumerable<PropertyAttribute<Column>> properties = GetPropertiesOfType<Column>(obj);

            foreach (PropertyAttribute<Column> property in properties)
            {
                Column column = property.Attribute;
                column.Value = property.Value;
                yield return column;
            }
        }

        private static IEnumerable<PropertyAttribute<TAttribute>> GetPropertiesOfType<TAttribute>(object obj) where TAttribute : Attribute
        {
            PropertyInfo[] properties = obj.GetType().GetProperties();

            foreach (PropertyInfo property in properties)
            {
                TAttribute attribute = property.GetCustomAttribute<TAttribute>(false);
                if (attribute != null)
                {
                    yield return new PropertyAttribute<TAttribute>(property.GetValue(obj), attribute, property);
                }
            }
        }

        private class PropertyAttribute<TAttribute>
        {
            public PropertyAttribute(object value, TAttribute attribute, PropertyInfo property)
            {
                Value = value;
                Attribute = attribute;
                Property = property;
            }

            public object Value { get; }
            public PropertyInfo Property { get; }
            public TAttribute Attribute { get; }
        }
    }
}
